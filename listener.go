package main

import (
	"fmt"
	"github.com/gorilla/mux"
	"log"
	"net/http"
	"strings"
)

type TankTankListener struct {
	callback func(action Action)
}

type Action struct {
	name string
}

func (l TankTankListener) processMessage(m string) {
	switch strings.Trim(m, "\n") {
	case "new_game":
		l.callback(Action{ "new_game" })
	}
}

func (l TankTankListener) echo(writer http.ResponseWriter, request *http.Request) {
	conn, err := upgrader.Upgrade(writer, request, nil)

	if err != nil {
		fmt.Println("upgrade:", err)
		return
	}

	defer conn.Close()

	for {
		mt, message, err := conn.ReadMessage()
		if err != nil {
			fmt.Println("read:", err)
			break
		}
		fmt.Printf("recv: %s", message)
		l.processMessage(string(message))
		err = conn.WriteMessage(mt, message)
		if err != nil {
			fmt.Println("write:", err)
			break
		}
	}
}

func (l TankTankListener) createGame(w http.ResponseWriter, r *http.Request) {
	l.callback(Action{ "new_game" })
}

func (l TankTankListener) ServerListen(callback func(action Action)) {
	l.callback = callback
	fmt.Println("Listening on: localhost:3000")
	router := mux.NewRouter().StrictSlash(true)
	router.HandleFunc("/new", l.createGame).Methods("POST")
	router.HandleFunc("/", l.echo)
	log.Fatal(http.ListenAndServe(*addr, router))
}
