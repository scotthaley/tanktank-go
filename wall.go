package main

type Wall struct {
    Top int
    Bottom int
    Left int
    Right int
}

func (w *Wall) CheckCollision(x float64, y float64, size float64, grid float64) bool {
    if x + size / 2 > float64(w.Left) * grid && x - size / 2 < float64(w.Right) * grid {
        if y + size / 2 > float64(w.Top) * grid && y - size / 2 < float64(w.Bottom) * grid {
            return true
        }
    }
    return false
}

func CheckWallCollisions(walls []*Wall, x float64, y float64, size float64, grid float64) *Wall {
    for _, w := range walls {
        if w.CheckCollision(x, y, size, grid) {
            return w
        }
    }
    return nil
}