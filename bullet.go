package main

import "math"

type Bullet struct {
    PositionX float64
    PositionY float64
    rot float64
    owner *Player
    active bool
    bounces int
}

const (
    bounceCount = 1
)

func NewBullet(p *Player) *Bullet {
    rads := p.Rot * (math.Pi / 180)

    return &Bullet{
        PositionX: p.PositionX + math.Cos(rads) * 20,
        PositionY: p.PositionY + math.Sin(rads) * 20,
        rot:       p.Rot,
        owner:     p,
        active:    true,
        bounces:   0,
    }
}

func (b *Bullet) BulletStep(g *TankTankGame) {
    rads := b.rot * (math.Pi / 180)

    newX := b.PositionX + math.Cos(rads) * 15
    newY := b.PositionY + math.Sin(rads) * 15

    hitPlayer := CheckPlayerCollisions(g.ActivePlayers(), newX, newY)

    if hitPlayer != nil {
        hitPlayer.Dead = true
        b.active = false
        b.owner.BulletCount--
        if hitPlayer != b.owner {
            b.owner.Kills ++
        } else {
            b.owner.SelfKilled = true
        }
    }

    hitWall := CheckWallCollisions(g.walls, newX, newY, 1, g.grid)

    if hitWall == nil {
        b.PositionX = newX
        b.PositionY = newY
    } else {
        b.bounces ++

        if b.PositionX > float64(hitWall.Right) * g.grid {
            b.rot = -(b.rot - 180)
        }
        if b.PositionX < float64(hitWall.Left) * g.grid {
            b.rot = 180 - b.rot
        }
        if b.PositionY < float64(hitWall.Top) * g.grid {
            b.rot = 180 - b.rot - 180
        }
        if b.PositionY > float64(hitWall.Bottom) * g.grid {
            b.rot = 360 - b.rot
        }

        if b.bounces > bounceCount {
            b.active = false
            b.owner.BulletCount--
        }
    }
}