package main

import (
    "encoding/json"
    "fmt"
    "github.com/gorilla/websocket"
    "math"
    "strings"
    "time"
)

type Player struct {
    Name string
    conn *websocket.Conn
    Active bool
    Dead bool
    PositionX float64
    PositionY float64
    Rot float64
    Kills int
    SelfKilled bool
    speed float64
    Size float64
    turning int
    gas int
    shooting bool
    shootTimeStamp int64
    BulletCount int
}

const (
    bulletMax int = 3
)

func NewPlayer(n string, c *websocket.Conn) *Player {
    return &Player{
        Name:      n,
        conn:      c,
        Active:    true,
        Dead:      false,
        speed:     2,
        Size:      20,
    }
}

func (p *Player) ResetPlayer() {
    p.speed = 2
    p.Size = 20
    p.Kills = 0
    p.SelfKilled = false
    p.turning = 0
    p.gas = 0
    p.shooting = false
    p.Dead = false
    p.BulletCount = 0
}

func (p *Player) PlayerStep(g *TankTankGame) {
    if p.turning == 1 {
        p.Rot -= 10
    } else if p.turning == 2 {
        p.Rot += 10
    }

    rads := p.Rot * (math.Pi / 180)

    newX := p.PositionX + math.Cos(rads) * float64(p.gas) * p.speed
    newY := p.PositionY + math.Sin(rads) * float64(p.gas) * p.speed

    if CheckWallCollisions(g.walls, newX, newY, p.Size, g.grid) == nil {
        p.PositionX = newX
        p.PositionY = newY
    }

    powerup := CheckPowerupCollisions(g.powerups, newX, newY, p.Size)
    if powerup != nil {
        powerup.ApplyPowerup(p)
        powerup.active = false
    }

    now := time.Now().UnixNano()

    if p.shooting && now - p.shootTimeStamp > 300000000 && p.BulletCount < bulletMax {
        g.spawnBullet(p)
        p.shootTimeStamp = now
        p.BulletCount++
    }
}

func (p *Player) CheckCollision(x float64, y float64) bool {
    if p.Dead {
        return false
    }
    first := math.Pow(p.PositionX - x, 2)
    second := math.Pow(p.PositionY - y, 2)
    return math.Sqrt(first + second) < p.Size
}

func CheckPlayerCollisions(players []*Player, x float64, y float64) *Player {
    for _, p := range players {
        if p.CheckCollision(x, y) {
            return p
        }
    }
    return nil
}

func (p *Player) Ping() {
    p.BroadcastBytes([]byte("Ping!"))
}

func (p *Player) Broadcast(m interface{})  {
    message, err := json.Marshal(m)
    if err != nil {
        fmt.Println("Could not marshal object", m)
    }
    p.BroadcastBytes(message)
}

func (p *Player) BroadcastBytes(m []byte) {
    err := p.conn.WriteMessage(websocket.BinaryMessage, m)
    if err != nil {
        fmt.Println("Error broadcasting message", err)
    }
}

func (p *Player) HandleMessage(m []byte, g *TankTankGame) {
    message := string(m)
    if strings.Contains(message, "|") {
        cmd := strings.Split(message, "|")
        var cmdOpt struct { Type string }
        json.Unmarshal([]byte(cmd[1]), &cmdOpt)
        switch cmd[0] {
        case "player:input-start":
            switch cmdOpt.Type {
            case "left":
                p.turning = 1
            case "right":
                p.turning = 2
            case "forward":
                p.gas = 2
            case "back":
                p.gas = -1
            case "shoot":
                p.shooting = true
            }
        case "player:input-end":
            switch cmdOpt.Type {
            case "turn":
                p.turning = 0
            case "gas":
                p.gas = 0
            case "shoot":
                p.shooting = false
            }
        }
    } else if message == "match:start" {
        g.StartGame()
    } else if message == "match:new" && g.scoreScreen {
        g.NewGame()
    }
}