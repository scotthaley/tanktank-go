package main

import (
    "fmt"
    "github.com/google/uuid"
    "math/rand"
    "time"
)

type TankTankGame struct {
    id string
    players []*Player
    bullets []*Bullet
    powerups []*Powerup
    walls []*Wall
    owner string
    started bool
    active bool
    width int
    height int
    grid float64
    tankSize float64
    scoreScreen bool
}

type GameListing struct {
    Id string
    PlayerCount int
    Players map[string]Player
    Bullets []Bullet
    Walls []Wall
    Powerups []Powerup
    Owner string
    Name string
    Started bool
    Active bool
    Width int
    Height int
    Grid float64
    ScoreScreen bool
}

func NewTankTankGame(owner string) *TankTankGame {
    return &TankTankGame{
        id:       uuid.New().String(),
        players:  make([]*Player, 0),
        bullets:  make([]*Bullet, 0),
        walls:    map1(),
        owner:    owner,
        active:   true,
        started:  false,
        height:   1000,
        width:    1000,
        grid:     50,
        tankSize: 20,
    }
}

func (g *TankTankGame) GetListing() GameListing {
    return GameListing{
        Id:          g.id,
        PlayerCount: g.PlayerCount(),
        Players:     g.playerMap(),
        Bullets:     g.bulletList(),
        Walls:       g.wallList(),
        Powerups:    g.powerupList(),
        Owner:       g.owner,
        Name:        fmt.Sprintf("%s's game", g.owner),
        Started:     g.started,
        Active:      g.active,
        Height:      g.height,
        Width:       g.width,
        Grid:        g.grid,
        ScoreScreen: g.scoreScreen,
    }
}

func (g *TankTankGame) NewGame() {
    g.started = false
    g.scoreScreen = false
    g.bullets = make([]*Bullet, 0)

    for _, p := range g.players {
        p.ResetPlayer()
        g.placePlayer(p)
    }
}

func (g *TankTankGame) GameStep() {
    if g.started {
        aliveCount := 0
        for _, p := range g.ActivePlayers() {
            if !p.Dead {
                aliveCount ++
                p.PlayerStep(g)
            }
        }
        for _, b := range g.bullets {
            if b.active {
                b.BulletStep(g)
            }
        }

        if aliveCount <= 1 {
            g.scoreScreen = true
        }

        s := rand.NewSource(time.Now().UnixNano())
        r := rand.New(s)

        if r.Float64() > 0.99 && len(g.powerupList()) < 3 {
            g.spawnPowerup()
        }
    }
}

func (g *TankTankGame) ActivePlayers() []*Player {
    var players = make([]*Player, 0)
    for _, p := range g.players {
        if p.Active {
            players = append(players, p)
        }
    }
    return players
}

func (g *TankTankGame) spawnPowerup() {
    p := RandomPowerup()

    s := rand.NewSource(time.Now().UnixNano())
    r := rand.New(s)

    testX := r.Float64() * float64(g.width)
    testY := r.Float64() * float64(g.height)

    for CheckWallCollisions(g.walls, testX, testY, 30, g.grid) != nil {
        testX = r.Float64() * float64(g.width)
        testY = r.Float64() * float64(g.height)
    }

    p.PositionX = testX
    p.PositionY = testY

    g.powerups = append(g.powerups, p)
}

func (g *TankTankGame) spawnBullet(p *Player) {
    b := NewBullet(p)
    g.bullets = append(g.bullets, b)
}

func (g *TankTankGame) playerMap() map[string]Player {
    var playerMap = make(map[string]Player)
    for _, p := range g.ActivePlayers() {
        playerMap[p.Name] = *p
    }
    return playerMap
}

func (g *TankTankGame) bulletList() []Bullet {
    var bullets = make([]Bullet, 0)
    for _, b := range g.bullets {
        if b.active {
            bullets = append(bullets, *b)
        }
    }
    return bullets
}

func (g *TankTankGame) wallList() []Wall {
    var walls = make([]Wall, 0)
    for _, w := range g.walls {
        walls = append(walls, *w)
    }
    return walls
}

func (g *TankTankGame) powerupList() []Powerup {
    var powerups = make([]Powerup, 0)
    for _, p := range g.powerups {
        if p.active {
            powerups = append(powerups, *p)
        }
    }
    return powerups
}

func (g *TankTankGame) BroadcastState() {
    listing := g.GetListing()
    for _, player := range g.ActivePlayers() {
        if player.Active {
            player.Broadcast(listing)
        }
    }
}

func (g *TankTankGame) PlayerCount() int {
    return len(g.ActivePlayers())
}

func (g *TankTankGame) PingPlayers() {
    for _, player := range g.ActivePlayers() {
        fmt.Println("Pinged player:", player.Name)
        player.Ping()
    }
}

func (g *TankTankGame) InactivePlayerExists(n string) *Player {
    for _, player := range g.players {
        if !player.Active && player.Name == n {
            return player
        }
    }
    return nil
}

func (g *TankTankGame) PlayerExists(n string) bool {
    for _, player := range g.ActivePlayers() {
        if player.Name == n { return true }
    }
    return false
}

func (g *TankTankGame) AddPlayer(p *Player) {
    existingPlayer := g.InactivePlayerExists(p.Name)
    if existingPlayer != nil {
        existingPlayer.conn = p.conn
        existingPlayer.Active = true
        return
    }

    g.placePlayer(p)

    if g.started {
        p.Dead = true
    }

    g.players = append(g.players, p)
    g.handlePlayerMessages(p)
}

func (g *TankTankGame) placePlayer(p *Player) {
    s := rand.NewSource(time.Now().UnixNano())
    r := rand.New(s)

    testX := r.Float64() * float64(g.width)
    testY := r.Float64() * float64(g.height)

    for CheckWallCollisions(g.walls, testX, testY, p.Size, g.grid) != nil {
        testX = r.Float64() * float64(g.width)
        testY = r.Float64() * float64(g.height)
    }

    p.PositionX = testX
    p.PositionY = testY
    p.Rot = r.Float64() * 360
}

func (g *TankTankGame) handlePlayerMessages(p *Player) {
    defer g.removePlayer(p)

    for {
        _, message, err := p.conn.ReadMessage()
        if err != nil {
            break
        }
        p.HandleMessage(message, g)
    }
}

func (g *TankTankGame) removePlayer(p *Player) {
    p.conn.Close()
    p.Active = false

    if len(g.ActivePlayers()) == 0 {
        g.active = false
        return
    }

    if g.owner == p.Name {
        g.owner = g.ActivePlayers()[0].Name
    }
}

func (g *TankTankGame) StartGame() {
    g.started = true
}