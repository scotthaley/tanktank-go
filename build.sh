sudo rm /var/app/current/go.*

go get github.com/google/uuid
go get github.com/gorilla/handlers
go get github.com/gorilla/mux
go get github.com/gorilla/websocket
go build -o bin/application -x tanktank-server-go
