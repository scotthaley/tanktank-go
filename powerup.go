package main

import (
    "math"
    "math/rand"
    "time"
)

type Powerup struct {
    PositionX float64
    PositionY float64
    Type string
    active bool
}

func RandomPowerup() *Powerup {
    var pType string
    s := rand.NewSource(time.Now().UnixNano())
    r := rand.New(s).Float64()

    switch {
    case r < 0.5:
        pType = "speed"
    default:
        pType = "size"
    }

    return &Powerup{
        Type:   pType,
        active: true,
    }
}

func (p *Powerup) ApplyPowerup(player *Player) {
    s := rand.NewSource(time.Now().UnixNano())
    r := rand.New(s)

    switch p.Type {
    case "speed":
        if r.Float64() > 0.8 {
            player.speed = 1
        } else {
            player.speed = 4
        }
    case "size":
        if r.Float64() > 0.8 {
            player.Size = 32
        } else {
            player.Size = 15
        }

    }
}

func (p *Powerup) CheckCollision(x float64, y float64, tankSize float64) bool {
    if !p.active {
        return false
    }
    first := math.Pow(p.PositionX - x, 2)
    second := math.Pow(p.PositionY - y, 2)
    return math.Sqrt(first + second) < tankSize
}

func CheckPowerupCollisions(powerups []*Powerup, x float64, y float64, tankSize float64) *Powerup {
    for _, p := range powerups {
        if p.CheckCollision(x, y, tankSize) {
            return p
        }
    }
    return nil
}
