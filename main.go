package main

import (
    "encoding/json"
    "flag"
    "fmt"
    "github.com/gorilla/handlers"
    "github.com/gorilla/mux"
    "github.com/gorilla/websocket"
    "io/ioutil"
    "log"
    "net/http"
    "time"
)

var games map[string]*TankTankGame
var gameIds []string
var addr = flag.String("addr", "localhost:5000", "http service address")
var upgrader = websocket.Upgrader{
    CheckOrigin: checkOrigin,
}

func checkOrigin(r *http.Request) bool {
    return true
}

func createGame(w http.ResponseWriter, r *http.Request) {
    reqBody, err := ioutil.ReadAll(r.Body)
    if err != nil {
        fmt.Println("Error with post data in /new", err)
    }

    var postData struct { PlayerName string }
    json.Unmarshal(reqBody, &postData)

    g := NewTankTankGame(postData.PlayerName)
    games[g.id] = g
    gameIds = append(gameIds, g.id)

    json.NewEncoder(w).Encode(struct { Id string }{g.id})
}

func gameStatus(w http.ResponseWriter, r *http.Request) {
    reqBody, err := ioutil.ReadAll(r.Body)
    if err != nil {
        fmt.Println("Error with post data in /status", err)
    }

    var postData struct {
        GameId string
        Name string
    }
    json.Unmarshal(reqBody, &postData)

    g := games[postData.GameId]

    if g != nil && g.active {
        if !g.PlayerExists(postData.Name) {
            json.NewEncoder(w).Encode(games[postData.GameId].GetListing())
        } else {
            json.NewEncoder(w).Encode(struct { Error string } { Error: "Player Exists" })
        }
    } else {
        json.NewEncoder(w).Encode(struct { Error string } { Error: "No Game" })
    }
}

func getGames(w http.ResponseWriter, r *http.Request) {
    response := []GameListing{}

    for _, gameId := range gameIds {
        g := games[gameId]
        if g.active {
            response = append(response, g.GetListing())
        }
    }

    json.NewEncoder(w).Encode(struct { Games []GameListing } { response })
}

func joinGame(w http.ResponseWriter, r *http.Request) {
    gameId := mux.Vars(r)["gameId"]
    name := mux.Vars(r)["name"]

    conn, err := upgrader.Upgrade(w, r, nil)
    if err != nil {
        fmt.Println("upgrade:", err)
        return
    }

    g := games[gameId]

    if !g.PlayerExists(name) {
        newPlayer := NewPlayer(name, conn)
        g.AddPlayer(newPlayer)
    }
}

func testPing(w http.ResponseWriter, r *http.Request) {
    gameId := mux.Vars(r)["gameId"]
    game := games[gameId]
    game.PingPlayers()

    json.NewEncoder(w).Encode(struct {
        Success bool
        Players int
    } {
        true,
        game.PlayerCount(),
    })
}

func gameLoop() {
    ticker := time.NewTicker(50 * time.Millisecond)

    for _ = range ticker.C {
        for _, gameId := range gameIds {
            g := games[gameId]
            if g.active {
                g.GameStep()
                g.BroadcastState()
            }
        }
    }
}

func main() {
    games = make(map[string]*TankTankGame)
    go gameLoop()

    fmt.Println("Listening on: localhost:5000")
    router := mux.NewRouter().StrictSlash(true)
    router.HandleFunc("/api/new", createGame).Methods("POST")
    router.HandleFunc("/api/games", getGames).Methods("POST")
    router.HandleFunc("/api/status", gameStatus).Methods("POST")
    router.HandleFunc("/api/test-ping/{gameId}", testPing).Methods("POST")
    router.HandleFunc("/join/{gameId}/{name}", joinGame)

    allowedHeaders := handlers.AllowedHeaders([]string{"X-Requested-With", "Content-Type", "Content-Length", "Date", "X-Content-Type-Options"})
    allowedOrigins := handlers.AllowedOrigins([]string{"*"})
    allowedMethods := handlers.AllowedMethods([]string{"GET", "POST"})
    log.Fatal(http.ListenAndServe(*addr, handlers.CORS(allowedOrigins, allowedHeaders, allowedMethods, handlers.AllowCredentials())(router)))
}
